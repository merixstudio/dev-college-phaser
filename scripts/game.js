var cursors;                // zmienna z kursorami klawiatury
var car;                    // samochód
var map;                    // mapa
var mapLayers = {};         // warstwy mapy
var checkpoints = [];       // lista checkpointów
var currentCheckpoint = 0;  // aktualny checkpoint
var currentLap = 1;         // aktualne okrążenie
var laps = 2;               // ilość okrążeń
var labels = {};            // teksty
var timer;                  // pętla licznika czasu
var raceTime = 0;           // czas wyścigu
var background;				// tło z flagą
var currentSpeed = 0;       // prędkość samochodu
var maxSpeed = 300;         // maksymalna prędkość samochodu

var GameState = {
    preload: function() {
        // W tej metodzie ładujemy wszystkie potrzebne assety
        game.load.image('car', 'assets/images/cars/car1_blue.png');

		game.load.image('intro', 'assets/images/intro.png');
		game.load.image('background', 'assets/images/bg.png');

        game.load.image('tiles', 'assets/images/tiles.png');
        game.load.tilemap('map', 'assets/maps/map1.json', null, Phaser.Tilemap.TILED_JSON);
    },

    create: function() {
        // Metoda create uruchamia się zaraz po uruchomieniu stanu, 
        // możemy w niej zdefiniować zmienne, które będziemy w trakcie gry wykorzystywać

        // Aktywujemy silnik fizyczny Arcade
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // Przypisanie do zmiennej kursorów klawiatury jako kontrolera dla gry
        cursors = game.input.keyboard.createCursorKeys();

        // Tworzymy mapę
        this.createMap();

        // Tworzymy samochód
        this.createCar();

        // Kamera podąża za samochodem       
        game.camera.follow(car);

        // Ekran startowy
        this.createIntro();

    },

    createCar: function() {
        car = game.add.sprite(590, 225, 'car');	// tworzymy sprite o id 'car' na pozycji x: 200 y: 200
        car.anchor.setTo(0.4, 0.5);				// ustawiamy środek obrotu
        game.physics.enable(car);				// włączamy fizykę dla obiektu
        car.body.collideWorldBounds = true;		// włączamy kolizję z krawędziami ekranu
        car.body.setSize(10, 10, 10, 5);        // zmieniamy rozmiar obszaru kolizji

        // Maksymalny kąt obrotu kierownicą
	    car.body.maxAngular = 200;

	    car.body.maxVelocity.x = maxSpeed;
	    car.body.maxVelocity.y = maxSpeed;
    },

    createMap: function() {
        map = game.add.tilemap('map'); 	// wczytujemy tilemapę do gry
        map.addTilesetImage('tiles'); 	// przypisujemy grafikę do tilemapy

        // Dodajemy warstwy mapy
        mapLayers['grass'] = map.createLayer('Grass');
        mapLayers['road'] = map.createLayer('Road');
        mapLayers['collision'] = map.createLayer('Collision');

        mapLayers['road'].resizeWorld(); // świat przyjmuje rozmiary warstwy

        map.setCollisionBetween(1, 100, true, mapLayers['collision']);
        //mapLayers['collision'].debug = true;

        this.parseCheckpoints(mapLayers['road'].layer.properties);
    },

    createLabels: function() {
	    // Licznik checkpointów
        labels.checkpoints = game.add.text(20, 20, "Checkpoint " + currentCheckpoint + "/" + checkpoints.length, { font: "24px Comic Sans MS", fill: "#ffffff", align: "left" });
	    labels.checkpoints.fixedToCamera = true;
	    labels.checkpoints.anchor.x = 0;

        // Licznik czasu
        labels.timer = game.add.text(game.width / 2, 20, "Time: 00:00:00", {font: "24px Comic Sans MS", fill: "#ffffff", align: "left"});
	    labels.timer.fixedToCamera = true;
        labels.timer.setTextBounds(-90, 0, 180, 100);

        // Licznik okrążeń
	    labels.laps = game.add.text(game.width - 20, 20, "Laps " + currentLap + "/" + laps, { font: "24px Comic Sans MS", fill: "#ffffff", align: "right" });
	    labels.laps.fixedToCamera = true;
	    labels.laps.anchor.x = 1;
    },

    timerFormat: function() {
        raceTime++;

        var date = new Date(null);
        date.setSeconds(this.raceTime); // ustawiamy sekundy dla obiektu date
        
        labels.timer.setText("Time: " + date.toISOString().substr(11, 8)); // wyświetlamy czas wyścigu na liczniku
    },

    createIntro() {
		setTimeout(function() {
			game.paused = true; // zatrzymujemy grę
		});
		background = game.add.sprite(0, 0, 'background');
		background.fixedToCamera = true;
		labels.splash = game.add.sprite(0, 0, 'intro');
		labels.splash.fixedToCamera = true;
		labels.intro = game.add.text(game.width / 2, 670, "Press UP to START!", { font: "24px Titillium Web", fill: "#ffffff", align: "center" });
		labels.intro.fixedToCamera = true;
		labels.intro.anchor.setTo(0.5);

		cursors.up.onDown.add(this.start, this); // dodajemy event na klawisz
	},

    start() {
		// Wyłączamy pauze
		game.paused = false;

		// Usuwamy intro
		labels.intro.kill();
		labels.splash.kill();
		background.visible = false;
			
		// Tworzymy teksty
		this.createLabels();

		// Pętla licznika
		timer = game.time.events.loop(10, this.timerFormat);

		// usuwamy event przypisany do klawisza
		cursors.up.onDown.remove(this.start, this);
	},

    parseCheckpoints: function(properties) {
        // pętla po właściwościach warstwy i wrzucamy wartości x i y do tablicy checkpoints
        for (var tile in properties) {
			var position = properties[tile].split(',');
			checkpoints.push({
				x: parseInt(position[0]),
				y: parseInt(position[1])
			})
		}
    },

    checkIfTileIsCheckpoint: function(x, y) {
        // sprawdzamy czy pozycja samochodu x, y znajduje się na pozycji checkpointa
		if (x === checkpoints[currentCheckpoint]['x'] && y === checkpoints[currentCheckpoint]['y']) {
		    this.nextCheckpoint(); // ustawiamy nowy checkpoint
		}
    },

    nextCheckpoint: function() {
		currentCheckpoint++; // wybieramy kolejny checkpoint

        // jeśli aktualny checkpoint jest ostatnim w tablicy sprawdzamy checkpointy od nowa
		if (currentCheckpoint === checkpoints.length) {
		    currentCheckpoint = 0;
			this.nextLap(); // nowe okrążenie
		}
        
		labels.checkpoints.setText("Checkpoint " + currentCheckpoint + "/" + checkpoints.length); // aktualizujemy wartość w liczniku
	},

    nextLap: function() {
		currentLap++; // zwiększamy licznik okrążeń

        // Kończymy grę, gdy aktualne okrążenie jest większe niż liczba zdefiniowanych okrążeń
		if (currentLap > laps) {
			this.finish(); // uruchamiamy funkcję kończącą grę
		} else {
			labels.laps.setText("Laps " + currentLap + "/" + laps); // aktualizujemy licznik
		}
	},

    finish: function() {
		background.visible = true; // odkrywamy tło z flagą

        // Dodajemy napis FINISH
		labels.finish = game.add.text(game.width / 2, 300, "FINISH!", {font: "72px Comic Sans MS", fill: "#ffffff", align: "center" });
		labels.finish.anchor.x = 0.5;
		labels.finish.fixedToCamera = true;
		labels.finish.setShadow(0, 0, 'rgba(0,0,0,0.5)', 15);

        // Wyświetlamy czas przejazdu
        labels.score = game.add.text(game.width / 2, 400, labels.timer.text, {font: "48px Comic Sans MS", fill: "#ffffff", align: "center" });
		labels.score.anchor.x = 0.5;
		labels.score.fixedToCamera = true;
		labels.score.setShadow(0, 0, 'rgba(0,0,0,0.5)', 15);

        // Ukrywamy niepotrzebne labele
        labels.checkpoints.visible = false;
        labels.timer.visible = false;
        labels.laps.visible = false;

		game.time.events.remove(this.timer); // Zatrzymujemy pętle odmierzającą czas wyświgu
		game.paused = true; // Zatrzymujemy grę
	},

    update: function() {
        // Główna pętla gry
        // Metoda uruchamiana jest z bardzo dużą częstotliwością (~60 fps)

        // Sterowanie samochodem
        this.controlCar();

        // Dodajemy kolizję pomiędzy obiektem 'car', a warstwą kolizji mapy
        game.physics.arcade.collide(car, mapLayers['collision']);

        // Sprawdzamy czy samochód znajduje się na checkpoincie
        this.checkIfTileIsCheckpoint(mapLayers['road'].getTileX(car.x), mapLayers['road'].getTileX(car.y));
    },

    controlCar: function() {
        // Zerujemy wartości gdy nie naciskamy klawiszy
        car.body.velocity.x = 0;		// zerujemy prędkość w osi x
        car.body.velocity.y = 0;		// zerujemy prędkość w osi y

        // Skręcanie samochodem - przyśpieszenie obrotu kierownicą
        if (currentSpeed != 0) { // skręcamy tylko gdy auto jest w ruchu
            if (cursors.left.isDown) {
                car.body.angularAcceleration = -600;
            }
            else if (cursors.right.isDown) {
                car.body.angularAcceleration = 600;
            }
            else {
                // zerujemy gdy nie skręcamy
                car.body.angularAcceleration = 0;
                car.body.angularVelocity = 0;
            }
        } else {
            // zatrzymujemy obrót gdy auto sie zatrzyma
            car.body.angularAcceleration = 0;
            car.body.angularVelocity = 0;
        }

        // Przyśpieszenie samochodu
        if (cursors.up.isDown) {
	    	currentSpeed += 5;
        } else if (cursors.down.isDown) { // hamowanie i wsteczny
	    	if (currentSpeed > 0) {
	    		currentSpeed -= 10; // gdy hamujemy szybko zmniejszamy prędkość
			} else {
				currentSpeed -= 2; // ujemna prędkość na wstecznym
			}
        } else {
            // jesli przestaniemy przyśpieszać auto zacznie zwalniać
            if (currentSpeed > 0) {
		    	currentSpeed -= 5;
				if (currentSpeed < 0) { // żeby auto nie zaczęło się cofać
					currentSpeed = 0;
				}
		    } else if (currentSpeed < 0) {
				currentSpeed += 5;
				if (currentSpeed > 0) {  // żeby auto nie zaczęło jechać do przodu
					currentSpeed = 0;
				}
			}
        }

        // Okreslamy maksymalną prędkość
        if (currentSpeed >= maxSpeed) {
    		currentSpeed = maxSpeed;
        } else if (currentSpeed <= -(maxSpeed/2)) {
			currentSpeed = -(maxSpeed/2);
		}

        // Sprawdzamy czy auto znajduje się na asfalcie
        var currentTile = map.getTile(mapLayers['road'].getTileX(car.body.x), mapLayers['road'].getTileY(car.body.y), 'Road');
	    if (currentTile == null) {
            // jeśli auto się przemieszcza spowalniamy je trzykrotnie
	    	if (currentSpeed != 0) {
    			game.camera.shake(0.001, 100); // trzęsienie kamerą gdy auto jeździ po trawie
	    	}

            // gdy auto jedzie za szybko spowalniamy je
            if (currentSpeed > 100) {
                currentSpeed -= 20;
            } else if (currentSpeed < -100) {
                currentSpeed += 20;
            }
	    }

        // Wprawiamy samochód w ruch
        car.body.velocity.copyFrom(game.physics.arcade.accelerationFromRotation(car.rotation, currentSpeed, car.body.acceleration));
    },

    render: function() {
        // Metoda uruchamiana po metodzie update

        // Car debug
        // game.debug.spriteInfo(car, 32, 32);
        // game.debug.text('Velocity: ' + car.body.velocity, 32, 128);
        // game.debug.text('angularVelocity: ' + car.body.angularVelocity, 32, 148);
	    // game.debug.text('angularAcceleration: ' + car.body.angularAcceleration, 32, 168);
	    // game.debug.text('Acceleration: ' + car.body.acceleration, 32, 188);
	    // game.debug.text('Velocity: ' + car.body.velocity, 32, 208);
	    // game.debug.text('currentSpeed: ' + currentSpeed, 32, 228);
        // game.debug.body(car);
    }
};